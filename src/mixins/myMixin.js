export default{
      data() {
        return {
          new_todo:'',
          edit_items:'',
          edit_id:'',
          inputValue: '',
          msg2: "Assalamualaikum",
          msg3: "Temen-temen",
          todos: [
            { 	id: 1, message: "Belajar", status:""},
            { 	id: 2, message: "Membaca", status:""}
          ],
          warning:'',
          showStyle: true
        }
      },
    computed: {
        msg4(){
            return "Ini computed"
        },
        numberList: function(){
            return this.index + 1;
        }
    },
    methods: {
        showNewMsg() {
            this.msg2 = 'pesan baruu'
        },
        checked(id){
          const tmpTodo = this.todos.find(todo => todo.id == id);
          if(tmpTodo.status == 'done'){
            tmpTodo.status = ''
          } else {
            tmpTodo.status = 'done'
          }
        },
        addTodo(){
          var numb = this.todos.slice(-1).pop() ? this.todos.slice(-1).pop().id : 0;      
          // console.log('add todo', this.inputValue)
          this.todos.push({
            id : numb + 1,
            message : this.inputValue,
            status : null
          })
          this.inputValue=''
          // console.log('isi todos:', this.todos)
        },
        editTodo(todos){
          this.edit_items = todos[1];
          this.edit_id = todos[0];
        },
        hapusTodo(index){
          this.todos.splice(index, 1)
        },
        save(data_edit){
                const tmpTodo = this.todos.find(todo => todo.id === this.edit_id);
          tmpTodo.message = data_edit;
          this.edit_items = ''
            },
            cancel(){
                this.edit_items = '';
                this.edit_id = '';
            },
      },
    watch: {
        inputValue: function (){
          if(this.inputValue.length > 10){
            this.warning = 'Maksimal 10 Karakter!'
          } else {
            this.warning=''
          }
        }
      }
}